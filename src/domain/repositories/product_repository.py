class ProductRepository:
    def create_product(self, product):
        return product
    
    def get_products(self):
        return [
            {"id":1, "name":"product 1", "price":100},
            {"id":2, "name": "product 2", "price": 50}
        ]
