import requests
import json
class CurrencyConversionService:
    def conversion(self, currencyFrom, currencyTo):
        url = f'http://localhost:1080/{currencyFrom}/to/{currencyTo}'
        response = requests.get(url)
        rate = json.loads(response.text)
        return rate.get("rate")


if __name__== "__main__":
    currency_service = CurrencyConversionService()
    result = currency_service.conversion("USD", "COP")
    print(result)   
    result = currency_service.conversion("USD", "CLP")
    print(result)   
    result = currency_service.conversion("USD", "BRL")
    print(result)   