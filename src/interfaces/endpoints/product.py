import json
# from src.application.dto.product import ProductDTO
from src.application.services.product_service import ProductService
productService = ProductService()

def create_product(event, context):
    try:
        data = json.loads(event["body"])
        product = productService.create_product(data)
        return {
            "statusCode":200,
            "body":json.dumps(product)
        }
    except Exception as e:
        return {
            "statusCode":500,
            "body":json.dumps("Error!")
        }
    
def get_products(event, context):
    print("get_products run...", )
    countryCode = event['queryStringParameters']['countryCode']
    products = productService.get_products(countryCode)
    return {
        "statusCode":200,
        "body": json.dumps(products)
    }