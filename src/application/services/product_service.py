from src.domain.repositories.product_repository import ProductRepository
from src.integrations.currencies_conversion.main import CurrencyConversionService
productRepository = ProductRepository()
currencyConversionService = CurrencyConversionService()

class ProductService:
    def create_product(self, product):
        return productRepository.create_product(product)
    def get_products(self, countryCode):
        products = productRepository.get_products()
        # integrar currency conversion
        currencies = {
            "CO":"COP",
            "CL":"CLP",
            "BR":"BRL",
        }
        print("rate", currencyConversionService.conversion("USD", currencies[countryCode]))
        rate = currencyConversionService.conversion("USD", currencies[countryCode])
        for product in products:
            product["price"] = product["price"] * rate
        return products
